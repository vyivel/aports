# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=dasel
pkgver=1.20.1
pkgrel=0
pkgdesc="Query and modify data structures using selector strings"
url="https://daseldocs.tomwright.me/"
license="MIT"
arch="all"
makedepends="go"
source="https://github.com/TomWright/dasel/archive/v$pkgver/dasel-$pkgver.tar.gz"

export GOPATH="$srcdir"
export GOFLAGS="$GOFLAGS -trimpath -modcacherw"
export CGO_ENABLED=0

build() {
	go build \
		-ldflags "-X github.com/tomwright/dasel/internal.Version=$pkgver" \
		-v \
		./cmd/dasel
}

check() {
	go test ./...
}

package() {
	install -Dm755 dasel "$pkgdir"/usr/bin/dasel
}

sha512sums="
6bd2d0d4ff95ed6e081aa7da12530e9be3bfcbb709c1fdb306b00883dc4f1579ccb3cfa882ac317dc971dab486224df56248597fba95936a325584ac7dcd031a  dasel-1.20.1.tar.gz
"
